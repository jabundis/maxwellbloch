import numpy as np
import Maxwell as MAX

def isArray(array):
    """
    Verifies that a ndarray is a ndarray
    """
    return hasattr(array, 'shape')

class Maxwell:
    def __init__(self, N, simTime, scenario, event, numLevels=3, method='Lax-Wendroff'):
        self.N = N
        self.simTime = simTime 
        self.scenario = scenario
        self.event = event
        self.numLevels = numLevels
        self.method = method

        self.update_x() 
        self.E = None

#    def __copy__(self):
#        return Maxwell( self.N, self.simTime, self.scenario, self.event, self.numLevels, self.method )

    def update_x(self):
        length = self.scenario.laser.get_length()
        M = self.event.get_M()
        self.x = np.linspace(0,1,M+1) * length

    def get_refIndex(self):
        return self.scenario.get_refIndex()

    def get_M(self):
        return self.event.get_M()

    def get_N(self):
        return self.N

    def get_simTime(self):
        return self.simTime

    def get_scenario(self):
        return self.scenario

    def get_event(self):
        return self.event

    def get_numLevels(self):
        return self.numLevels

    def get_method(self):
        return self.method

    def get_x(self):
        return self.x

    def get_t(self):
        t = self.get_timeStep()/float(self.get_N()) * self.get_simTime()
        return t

    def get_E(self):
        """
        Updates attribute 'E' according to current Event and returns electric field
        """
        if(isArray(self.event.E_left) and isArray(self.event.E_right)):
            self.updateE()
        return self.E

    def get_timeStep(self):
        return self.event.get_timeStep()

#    def set_M(self, M):
#        assert(type(M) == int), 'number of spatial intervals must be an integer'
#        self.M = M

    def set_N(self, N):
        assert(type(N) == int), 'number of time intervals must be an integer'
        self.N = N

    def set_simTime(self, simTime):
        self.simTime = simTime

    def set_scenario(self, scenario):
        self.scenario = scenario
        self.update_x()

    def set_event(self, event):
        self.event = event 
        self.update_x()

    def set_numLevels(self, numLevels):
        assert(type(numLevels) == int), 'Number of levels must be an integer'
        self.numLevels = numLevels

    def set_method(self, method):
        self.method = method

    def set_initial_E_left(self, dist=None, loc=None, scale=None):
        E_left = MAX.initial_E(self.get_M()+2, dist, loc, scale)
        event = self.get_event()
        event.set_E_left( E_left )
        event.set_timeStep( 0 )

    def set_initial_E_right(self, dist=None, loc=None, scale=None):
        E_right = MAX.initial_E(self.get_M()+2, dist, loc, scale)
        event = self.get_event()
        event.set_E_right( E_right )
        event.set_timeStep( 0 )

    def set_initial_E(self, dist=None, loc=None, scale=None ):
        """
        Sets initial E_left and E_right according to a distribution 'dist'
        and its parameters. 

        It makes sure that: 
            E_right=r1*E_left at x=0 and E_left=r2*E_right at x=M.
        """
        self.set_initial_E_left(dist, loc, scale)
        self.set_initial_E_right(dist, loc, scale)
        self.event.E_right[1] = self.scenario.r1 * self.event.E_left[1]  
        self.event.E_left[-2] = self.scenario.r2 * self.event.E_right[-2]

    def updateE(self):
        self.E = self.electricField()

    def fieldAmplitudesUpdate(self):
        """
        Updates right and left field amplitudes if 'self.event' is set
        using the method 'MAX.fieldAmplitudesUpdate_v2'
        """
        if(self.event.isSet()):
            E_left = self.event.get_E_left()
            E_right = self.event.get_E_right()
            F = self.event.get_F()
            DtF = self.event.get_DtF()
            r1 = self.scenario.get_r1()
            r2 = self.scenario.get_r2()
            deltaX = self.scenario.laser.get_length() / self.get_M()
            deltaT = self.get_simTime() / self.get_N()
            k = self.scenario.get_k()
            method = self.get_method()

            #Update electric field amplitudes
            E_left, E_right = MAX.fieldAmplitudesUpdate_v2(E_left, E_right, F, DtF, r1, r2, deltaX, deltaT, k, 
                                                   method=method)
            #Update current state
            self.event.increaseTimeStep()
            self.event.set_E_left(E_left)
            self.event.set_E_right(E_right)
            self.event.set_F(None)
            self.event.set_DtF(None)
            self.event.updateEventStatus()

    def electricField(self):
        t = self.get_t()

        if(isArray(self.event.E_left) and isArray(self.event.E_right)):
            E_left = self.event.get_E_left()[1:-1]
            E_right = self.event.get_E_right()[1:-1]
            E = MAX.electricField( E_left, E_right, self.get_x(), t, 
                                  self.scenario.get_w(), self.get_refIndex() )
            return E
        else:
            raise Exception("Field amplitudes must be provided in 'event'")




class Laser:
    def __init__(self, length, material):
        self.length = length
        self.material = material

    def get_length(self):
        return self.length

    def get_material(self):
        return self.material

    def set_length(self, length):
        self.length = length

    def set_material(self, material):
        self.material = material



class Scenario:
    def __init__(self, laser, k, r1, r2, freq):
        self.laser = laser
        self.k = k
        self.r1 = r1
        self.r2 = r2
        self.freq = freq

        self.w = 2*np.pi*freq
        self.update_refIndex()

    def get_laser(self):
        return self.laser

    def get_k(self):
        return self.k

    def get_r1(self):
        return self.r1

    def get_r2(self):
        return self.r2

    def get_freq(self):
        return self.freq

    def get_w(self):
        return self.w

    def get_refIndex(self):
        return self.refIndex

    def set_laser(self, laser):
        self.laser = laser

        self.update_refIndex()


    def set_k(self, k):
        self.k = k

    def set_r1(self, r1):
        self.r1 = r1

    def set_r2(self, r2):
        self.r2 = r2

    def set_freq(self, freq):
        self.freq = freq

        self.w = 2*np.pi*freq
        self.update_refIndex()


    def set_w(self, w):
        self.w = w

        self.freq = w / (2*np.pi)
        self.update_refIndex()


    def set_refIndex(self, refIndex):
        self.refIndex = refIndex

    def update_refIndex(self):
        material = self.laser.get_material()
        material.set_f( [ self.get_freq() ] )
        self.refIndex = np.real( material.get_n()[0] )




class Event:
    def __init__(self, M, timeStep=None, E_left=None, E_right=None, F=None, DtF=None):
        assert(type(M) == int and M>0), 'number of spatial intervals must be an integer greater than 0'
        self.M = M   #Number of intervals in simulation window
#        self.timeStep = timeStep
#        self.E_left = E_left
#        self.E_right = E_right
#        self.F = F
#        self.DtF = DtF
        self.set_timeStep(timeStep)
        self.set_E_left(E_left)
        self.set_E_right(E_right)
        self.set_F(F)
        self.set_DtF(DtF)
        self.eventSet = self.isComplete()

    def get_M(self):
        return self.M

    def get_timeStep(self):
        return self.timeStep

    def get_E_left(self):
        return self.E_left

    def get_E_right(self):
        return self.E_right

    def get_F(self):
        return self.F

    def get_DtF(self):
        return self.DtF

    def set_timeStep(self, timeStep):
        assert(type(timeStep) == int or timeStep==None), 'time step must be an integer'
        self.timeStep = timeStep
        self.set_eventSet(False)

    def set_M(self, M):
        """
        After setting 'M' all other event attributes are set to None (initializes a new Event).
        """
        assert(type(M) == int and M>0), 'number of spatial intervals must be an integer greater than 0'
        self.M = M
        self.timeStep = None
        self.E_left = None
        self.E_right = None
        self.F = None
        self.DtF = None
        self.eventSet = False

    def set_E_left(self, E_left):
        """
        It assumes you provide one extra point beyond each simulation window boundary
        (i.e., M + 3 points in total).
        """
        if(isArray(E_left)): self.checkArrayDim(E_left)
        self.E_left = E_left
        self.set_eventSet(False)

    def set_E_right(self, E_right):
        """
        It assumes you provide one extra point beyond each simulation window boundary
        (i.e., M + 3 points in total).
        """
        if(isArray(E_right)): self.checkArrayDim(E_right)
        self.E_right = E_right
        self.set_eventSet(False)

    def set_F(self, F):
        """
        It assumes you provide one extra point beyond each simulation window boundary
        (i.e., M + 3 points in total).
        """
        if(isArray(F)): self.checkArrayDim(F)
        self.F = F
        self.set_eventSet(False)

    def set_DtF(self, DtF):
        """
        It assumes you provide one extra point beyond each simulation window boundary
        (i.e., M + 3 points in total).
        """
        if(isArray(DtF)): self.checkArrayDim(DtF)
        self.DtF = DtF
        self.set_eventSet(False)

    def set_eventSet(self, status):
        self.eventSet = status

    def isComplete(self):
        if(self.timeStep!=None and isArray(self.E_left) and isArray(self.E_right) and isArray(self.F) and isArray(self.DtF)):
            return True
        return False

    def isSet(self):
        return self.eventSet

    def updateEventStatus(self):
        self.eventSet = self.isComplete()

    def increaseTimeStep(self):
        self.timeStep += 1

    def checkArrayDim(self, array):
        if(len(array) != self.M+3): 
            raise Exception('Array length must equal M + 3 (one element beyong each simulation window edge)')
